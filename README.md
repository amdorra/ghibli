# Install dependencies

pip install -r requirements.txt

# Run tests

python -m pytest

# Run the server

python -m ghibli.run
