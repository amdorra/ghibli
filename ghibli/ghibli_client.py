from cachetools import cached, TTLCache
from typing import List, Dict, Any
import logging
import requests

logger = logging.getLogger(__name__)

_CACHE = TTLCache(maxsize=1024, ttl=60)
# I'm using 250 since it is the maximum, the data was also much less than
#  and I didn't need to do any pagination here
_RESULTS_LIMIT = 250

MOVIES_URL = "https://ghibliapi.herokuapp.com/films"
PEOPLE_URL = "https://ghibliapi.herokuapp.com/people"


class Person:
    def __init__(
        self, id: str = None, name: str = None, movie_urls: List[str] = None,
    ):
        self.id = id
        self.name = name
        self.movie_urls = movie_urls or []

    @classmethod
    def from_payload(cls, payload: Dict[str, Any]):
        return cls(
            id=payload.get("id"),
            name=payload.get("name"),
            movie_urls=payload.get("films", []),
        )

    def __eq__(self, other):
        return (
            self.id == other.id
            and self.name == other.name
            and self.movie_urls == other.movie_urls
        )


class Movie:
    def __init__(
        self,
        id: str = None,
        title: str = None,
        description: str = None,
        director: str = None,
        producer: str = None,
        release_date: str = None,
        rt_score: str = None,
        url: str = None,
        people: List[Person] = None,
    ):
        self.id = id
        self.title = title
        self.description = description
        self.director = director
        self.producer = producer
        self.release_date = release_date
        self.rt_score = rt_score
        self.url = url
        self.people = people

    @classmethod
    def from_payload(cls, payload: Dict[str, Any]):
        return cls(
            id=payload.get("id"),
            title=payload.get("title"),
            description=payload.get("description"),
            director=payload.get("director"),
            producer=payload.get("producer"),
            release_date=payload.get("release_date"),
            rt_score=payload.get("rt_score"),
            url=payload.get("url"),
        )

    # !TODO: test this
    def __eq__(self, other):
        return (
            self.id == other.id
            and self.title == other.title
            and self.description == other.description
            and self.director == other.director
            and self.producer == other.producer
            and self.release_date == other.release_date
            and self.rt_score == other.rt_score
            and self.url == other.url
            and self.people == other.people
        )


@cached(cache=_CACHE)
def get_movies() -> List[Movie]:
    """
    Returns a list of movies from ghibli API.

    people would be set to None, if the list of people couldn't be retrieved
    from ghibli API.
    """
    movies = [
        Movie.from_payload(movie_payload)
        for movie_payload in _get_response_json(MOVIES_URL)
    ]
    if not movies:
        return []

    try:
        people = get_people()
    except GhibliError:
        return movies

    _assign_people_to_movies(movies, people)

    return movies


def get_people() -> List[Person]:
    return [
        Person.from_payload(person_payload)
        for person_payload in _get_response_json(PEOPLE_URL)
    ]


def clear_cache():
    _CACHE.clear()


class GhibliError(Exception):
    pass


def _assign_people_to_movies(
    movies: List[Movie], people: List[Person]
) -> Dict[str, List[Person]]:
    movie_people_map = {}

    for person in people:
        for movie_url in person.movie_urls:
            if movie_people_map.get(movie_url):
                movie_people_map.get(movie_url).append(person)
            else:
                movie_people_map[movie_url] = [person]

    for movie in movies:
        movie.people = movie_people_map.get(movie.url, [])


# TODO: call this function asynchronously
def _get_response_json(url) -> List[Dict[str, Any]]:
    try:
        response = requests.get(url, params={"limit": 250})
    except (ConnectionError, requests.RequestException):
        logger.exception('Failed to retrieve data from "{}".'.format(url))
        raise GhibliError()

    if response.status_code != requests.codes.ok:
        logger.error(
            'Got {} status code when retrieving data from "{}".'.format(
                response.status_code, url
            )
        )
        raise GhibliError()

    # TODO: handle invalid json
    return response.json()
