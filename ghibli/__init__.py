import requests
from flask import Flask, render_template, redirect, url_for, abort

from . import ghibli_client as ghibli


def create_app(config=None):
    app = Flask(__name__, instance_relative_config=True)

    if config:
        app.config.from_mapping(config)

    @app.route("/")
    def index():
        return redirect(url_for("movies"))

    # I would move this to a seprate module if there were more endpoints
    # needed for the server
    @app.route("/movies")
    def movies():
        try:
            return render_template("movies.j2", movies=ghibli.get_movies())
        except ghibli.GhibliError:
            abort(requests.codes.service_unavailable)

    return app
