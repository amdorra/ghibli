#!/usr/bin/python3


from . import create_app


if __name__ == "__main__":
    # TODO! don't use a development server in production
    app = create_app({"ENV": "development"})
    app.run(port=8000, debug=True)
