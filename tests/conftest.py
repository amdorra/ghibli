import pytest
from ghibli import create_app, ghibli_client as ghibli


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    return app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()


@pytest.fixture
def cache():
    """
    Clears the cache after a test is done
    """
    yield
    ghibli.clear_cache()


@pytest.fixture
def movie():
    return {
        "id": "M1ID",
        "title": "Castle in the Sky",
        "description": "The orphan Sheeta",
        "director": "Hayao Miyazaki",
        "producer": "Isao Takahata",
        "release_date": "1986",
        "rt_score": "95",
        "people": ["https://ghibliapi.herokuapp.com/people/"],
        "url": "https://ghibliapi.herokuapp.com/films/M1ID",
    }


@pytest.fixture
def person_in_movie():
    return {
        "id": "ba924631-068e-4436-b6de-f3283fa848f0",
        "name": "Ashitaka",
        "gender": "Male",
        "age": "late teens",
        "eye_color": "Brown",
        "hair_color": "Brown",
        "films": ["https://ghibliapi.herokuapp.com/films/M1ID"],
        "url": "https://ghibliapi.herokuapp.com/people/",
    }


@pytest.fixture
def person_not_in_movie():
    return {
        "id": "ba924631-068e-4436-b6de-f3283fa848f0",
        "name": "Ashitaka",
        "gender": "Male",
        "age": "late teens",
        "eye_color": "Brown",
        "hair_color": "Brown",
        "films": ["https://ghibliapi.herokuapp.com/films/not-here"],
        "url": "https://ghibliapi.herokuapp.com/people/ba924631-068e-4436-b6de",
    }
