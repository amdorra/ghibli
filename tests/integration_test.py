import responses
import requests


@responses.activate
def test_get_movies_returns_ok(client, movie, person_in_movie, cache):
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/films",
        json=[movie],
        status=requests.codes.ok,
    )
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/people",
        json=[person_in_movie],
        status=requests.codes.ok,
    )
    response = client.get("/movies")
    assert response.status_code == requests.codes.ok
    response_body = str(response.data)
    assert movie["title"] in response_body
    assert person_in_movie["name"] in response_body


@responses.activate
def test_get_movies_returns_service_unavailable(client, movie, person_in_movie, cache):
    responses.add(
        responses.GET, json=[movie], status=requests.codes.not_found,
    )
    response = client.get("/movies")
    assert response.status_code == requests.codes.service_unavailable
