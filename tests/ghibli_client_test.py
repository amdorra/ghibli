from ghibli import ghibli_client as ghibli
from ghibli.ghibli_client import Person, GhibliError, get_movies
import requests
import responses
import pytest


@pytest.mark.parametrize(
    "a,b,are_equal",
    [
        (Person("1", "name", []), Person("1"), False),
        (Person("1", "name", []), Person("1", "name", []), True),
        (Person("1", "name", []), Person("1", "name2", []), False),
        (Person("1", "name", []), Person("1", "name", ["1"]), False),
    ],
)
def test_people_equal(a, b, are_equal):
    assert (a == b) == are_equal


# TODO: add tests for Movie.__eq__()


@responses.activate
def test_get_movies_returns_empty_list(cache):
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/films",
        json=[],
        status=requests.codes.ok,
    )
    assert len(ghibli.get_movies()) == 0
    assert len(responses.calls) == 1


@responses.activate
def test_get_movies_returns_a_movie(cache, movie):
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/films",
        json=[movie],
        status=requests.codes.ok,
    )
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/people",
        json=[],
        status=requests.codes.ok,
    )
    assert ghibli.get_movies() == [
        ghibli.Movie(
            id=movie["id"],
            title=movie["title"],
            description=movie["description"],
            director=movie["director"],
            producer=movie["producer"],
            release_date=movie["release_date"],
            rt_score=movie["rt_score"],
            url=movie["url"],
            people=[],
        )
    ]
    assert len(responses.calls) == 2


@responses.activate
def test_get_movies_returns_a_movie_and_a_person(cache, movie, person_in_movie):
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/films",
        json=[movie],
        status=requests.codes.ok,
    )
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/people",
        json=[person_in_movie],
        status=requests.codes.ok,
    )
    assert ghibli.get_movies() == [
        ghibli.Movie(
            id=movie["id"],
            title=movie["title"],
            description=movie["description"],
            director=movie["director"],
            producer=movie["producer"],
            release_date=movie["release_date"],
            rt_score=movie["rt_score"],
            url=movie["url"],
            people=[
                ghibli.Person(
                    id=person_in_movie["id"],
                    name=person_in_movie["name"],
                    movie_urls=person_in_movie["films"],
                )
            ],
        )
    ]
    assert len(responses.calls) == 2


@responses.activate
def test_get_movies_returns_a_movie_and_a_person_given2_people(
    cache, movie, person_in_movie, person_not_in_movie
):
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/films",
        json=[movie],
        status=requests.codes.ok,
    )
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/people",
        json=[person_in_movie, person_not_in_movie],
        status=requests.codes.ok,
    )
    assert ghibli.get_movies() == [
        ghibli.Movie(
            id=movie["id"],
            title=movie["title"],
            description=movie["description"],
            director=movie["director"],
            producer=movie["producer"],
            release_date=movie["release_date"],
            rt_score=movie["rt_score"],
            url=movie["url"],
            people=[
                ghibli.Person(
                    id=person_in_movie["id"],
                    name=person_in_movie["name"],
                    movie_urls=person_in_movie["films"],
                )
            ],
        )
    ]
    assert len(responses.calls) == 2


@responses.activate
def test_get_movies_raises_exception_if_ghilbi_response_not_200(cache):
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/films",
        status=requests.codes.not_found,
    )
    with pytest.raises(GhibliError):
        get_movies()


@responses.activate
def test_get_movies_raises_exception_there_was_a_connection_error(cache):
    responses.add(
        responses.GET, "https://ghibliapi.herokuapp.com/films", body=ConnectionError()
    )
    with pytest.raises(GhibliError):
        get_movies()


@responses.activate
def test_get_movies_returns_only_movies_if_people_response_not_200(cache, movie):
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/films",
        json=[movie],
        status=requests.codes.ok,
    )
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/people",
        status=requests.codes.bad_request,
    )
    assert ghibli.get_movies() == [
        ghibli.Movie(
            id=movie["id"],
            title=movie["title"],
            description=movie["description"],
            director=movie["director"],
            producer=movie["producer"],
            release_date=movie["release_date"],
            rt_score=movie["rt_score"],
            url=movie["url"],
        )
    ]


@responses.activate
def test_get_movies_returns_only_movies_if_people_raised_timeout(cache, movie):
    responses.add(
        responses.GET,
        "https://ghibliapi.herokuapp.com/films",
        json=[movie],
        status=requests.codes.ok,
    )
    responses.add(
        responses.GET, "https://ghibliapi.herokuapp.com/people", body=requests.Timeout()
    )
    movies = ghibli.get_movies()
    assert movies == [
        ghibli.Movie(
            id=movie["id"],
            title=movie["title"],
            description=movie["description"],
            director=movie["director"],
            producer=movie["producer"],
            release_date=movie["release_date"],
            rt_score=movie["rt_score"],
            url=movie["url"],
        )
    ]
    assert movies[0].people is None
